// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";

import Engine from "./Engine";
import { Command } from "./typings";

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log('Congratulations, your extension "jesttestext" is now active!');

  // The command has been defined in the package.json file
  // Now provide the implementation of the command with registerCommand
  // The commandId parameter must match the command field in package.json
  const commands: Command[] = [];
  const engine = new Engine();

  commands.push({
    name: "jesture.collapseAllTest",
    action: () => engine.collapseTests(),
  });

  for (let comm of commands) {
    let disp = vscode.commands.registerCommand(comm.name, comm.action);
    context.subscriptions.push(disp);
  }
}

// This method is called when your extension is deactivated
export function deactivate() {}
