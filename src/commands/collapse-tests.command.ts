import * as vscode from "vscode";
import { ExecCommand } from "../typings";

export default class CollapseTestsExecCommand implements ExecCommand {
  constructor() {
    this.exec = this.exec.bind(this);

    vscode.workspace.onDidOpenTextDocument((doc) => {
      const isFoldingEnabled = vscode.workspace
        .getConfiguration("jesture")
        .get("enableFoldOnOpen");
      isFoldingEnabled && this.exec(doc);
    });
  }

  exec(doc?: vscode.TextDocument) {
    // vscode.window.showInputBox({ title: "Hello World", validateInput });

    if (!doc) {
      const textEditor = vscode.window.activeTextEditor;
      doc = textEditor?.document;
    }

    // Return if not match any filename .spec.(js ext) or .test.(js ext),
    if (!doc || !doc.fileName.match(/.+\.(spec|test)\.[jt]sx?$/)) {
      console.log("Not a test file ending process");
      return;
    }

    this.foldRegions(doc);
  }

  private getRegions(doc: vscode.TextDocument): number[] {
    const lines = doc.getText().split("\n");
    const linesToFold: number[] = [];
    lines.forEach((l, i) => {
      if (l.replace(/(\t|\s)/g, "").match(/^(test\(|it\()/)) {
        linesToFold.push(i);
      }
    });

    return linesToFold;
  }

  private async foldRegions(doc: vscode.TextDocument): Promise<void> {
    const regions = this.getRegions(doc);
    console.log({ regions });
    await vscode.commands.executeCommand("editor.fold", {
      levels: 1,
      selectionLines: regions,
    });

    // vscode.languages.registerFoldingRangeProvider(
    //   ["javascript", "typescript"],
    //   new CustomFoldingRangeProvider()
    // );
  }
}
