import * as vscode from "vscode";
import * as path from "path";
import { ExecCommand } from "../typings";
import { Dirent, promises as fs } from "fs";

class CustomCompletionItemProvider implements vscode.CompletionItemProvider {
  protected shouldProvide(
    doc: vscode.TextDocument,
    pos: vscode.Position
  ): boolean {
    const lines = doc.getText().split("\n");

    const cursorLine = lines[pos.line];
    const markerGet = 'get("';

    if (cursorLine.indexOf(markerGet) < 0) {
      return false;
    }

    if (
      cursorLine.slice(pos.character - markerGet.length, pos.character) !==
      markerGet
    ) {
      return false;
    }

    return true;
  }

  protected async _getAbsoluteFilePathsFromDir(uri: string) {
    const subDir = await fs.readdir(uri, { withFileTypes: true });

    return Promise.all(
      subDir.map(async (el): Promise<any> => {
        if (el.isFile()) {
          return path.resolve(uri, el.name);
        }
        const relativePaths = await this._getAbsoluteFilePathsFromDir(
          `${uri}/${el.name}`
        );
        return relativePaths;
      })
    );
  }

  protected async getAbsoluteFilePathsFromDir(uri: string): Promise<string[]> {
    return (await this._getAbsoluteFilePathsFromDir(uri)).flat(10);
  }

  protected async getCompletionItems(
    activeTextEditor: vscode.TextEditor
  ): Promise<vscode.CompletionItem[]> {
    const stubsPath = path.resolve(
      activeTextEditor.document.fileName,
      "../../__stubs__"
    );

    const absoluteFilePaths = await this.getAbsoluteFilePathsFromDir(stubsPath);
    const stubFileUris = absoluteFilePaths.map((afp) => {
      return afp
        .replace(stubsPath, "")
        .replace(/\\+/g, "/")
        .replace(/^\/([\w\-\/]+)\.json/, "$1");
    });
    return stubFileUris.map((sfu) => ({
      label: sfu,
    }));
  }

  async provideCompletionItems(
    doc: vscode.TextDocument,
    pos: vscode.Position
  ): Promise<vscode.CompletionItem[]> {
    console.log("In provideCompletionItems");
    const defaultResult: vscode.CompletionItem[] = [];

    if (!this.shouldProvide(doc, pos)) {
      return defaultResult;
    }

    const activeTextEditor = vscode.window.activeTextEditor;
    if (!activeTextEditor) {
      return defaultResult;
    }

    return await this.getCompletionItems(activeTextEditor);
  }
}

export default class AutoCompleteStubsExecCommand implements ExecCommand {
  constructor() {
    vscode.languages.registerCompletionItemProvider(
      ["javascript", "typescript"],
      new CustomCompletionItemProvider(),
      ...['"']
    );
  }

  exec() {}
}
