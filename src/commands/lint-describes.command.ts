import * as vscode from "vscode";

import Line from "../shared/helpers/line.helper";

import { ExecCommand } from "../typings";

export type CreateWarningDiagnosticOpts = {
  name?: string;
  from: [number, number];
  to: [number, number];
  type?: vscode.DiagnosticSeverity;
  msg: string;
};

export default class LintDescribesCommand implements ExecCommand {
  protected collectionName: string = "lint-jest-describe";
  protected diagnosticCollection: vscode.DiagnosticCollection;
  protected diagnostics: vscode.Diagnostic[] = [];
  constructor() {
    this.diagnosticCollection = vscode.languages.createDiagnosticCollection(
      this.collectionName
    );

    vscode.window.onDidChangeActiveTextEditor(() => {
      this.exec();
    }, null);

    vscode.window.onDidChangeWindowState((state) => {
      if (state.focused) {
        this.exec();
      }
    });

    vscode.workspace.onDidOpenTextDocument(() => {
      this.exec();
    });

    vscode.workspace.onDidSaveTextDocument(() => {
      this.exec();
    });
  }

  protected createWarningDiagnostic(
    doc: vscode.TextDocument,
    { name, from, to, type, msg }: CreateWarningDiagnosticOpts
  ) {
    const diagnostic = new vscode.Diagnostic(
      new vscode.Range(...from, ...to),
      msg,
      type || vscode.DiagnosticSeverity.Warning
    );
    this.diagnostics.push(diagnostic);
    this.diagnosticCollection.set(doc.uri, [diagnostic]);
  }

  protected removeWarningDiagnostics(): void {
    this.diagnosticCollection.clear();
  }

  exec() {
    const doc = vscode.window.activeTextEditor?.document;
    if (!doc) {
      return;
    }
    const text = doc.getText();
    const lines = text.split("\n");

    this.removeWarningDiagnostics();

    lines.forEach((l, idx) => {
      const line = new Line(l);
      const isDesbribeLine = line.trimSpace().match(/^describe\(/);
      if (!isDesbribeLine) {
        return;
      }

      const [start, end] = line.findQuotes();
      if (!start || !end) {
        return;
      }

      const testDescription = l.slice(start + 1, end);
      console.log({ testDescription });
      if (!testDescription.match(/(\w+): \[(?:([\w-]+),? ?)+\]/)) {
        this.createWarningDiagnostic(doc, {
          from: [idx, start - 1],
          to: [idx, end + 1],
          msg: `Format for describing test is not correct, It should match following pattern :
  <Id>: [<class1>, <class2>, ...]`,
        });
      }
    });
  }
}
