export default class Line {
  constructor(protected line: string) {}

  findQuotes(): number[] {
    const quotes = [];
    // Iterate through the string
    for (let i = 0; i < this.line.length; i++) {
      // Check if the current character is a quote
      if (this.line[i] === '"') {
        quotes.push(i);
      }
    }
    return quotes;
  }

  trimSpace(): string {
    return this.line.replace(/(\t|\s)/, "");
  }
}
