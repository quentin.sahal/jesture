type Exec = (args: any) => void;

export interface ExecCommand {
  exec: Exec;
}

export type Command = {
  name: string;
  action: Exec;
};
