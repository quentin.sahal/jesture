import CollapseTestsCommand from "./commands/collapse-tests.command";
import AutocompleteStubsCommand from "./commands/autocomplete-stubs.command";
import LintDescribesCommand from "./commands/lint-describes.command";

export default class Engine {
  constructor(
    protected collapseTestsCommand = new CollapseTestsCommand(),
    protected autocompleteStubsCommand = new AutocompleteStubsCommand(),
    protected lintDescribesCommand = new LintDescribesCommand()
  ) {}

  collapseTests() {
    return this.collapseTestsCommand.exec();
  }

  lintDescribes() {
    return this.lintDescribesCommand.exec();
  }
}
